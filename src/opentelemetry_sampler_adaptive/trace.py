"""Implementation of samplers for opentelemetry."""
import math
import sys
import time
from abc import ABC, abstractmethod
from collections import Counter
from typing import Dict, Hashable, List, Optional, Sequence, Any, Tuple
from types import MappingProxyType

from opentelemetry import trace
from opentelemetry.sdk.trace.sampling import SamplingResult, Decision, Sampler
from opentelemetry.context import Context
from opentelemetry.trace.span import TraceState
from opentelemetry.util.types import Attributes

HASH_MASK = (1 << sys.hash_info.width) - 1


def should_keep(*, trace_id: Hashable, sample_rate: float) -> bool:
    """Sample deterministically based on a trace id.

    This is essentially how beeline-python implements its standard
    sampling.

    """

    hash_value = hash(trace_id) & HASH_MASK
    sample_upper_bound = HASH_MASK / sample_rate
    return hash_value < sample_upper_bound


class CustomSampler(ABC):
    """Base class for custom samplers."""
    # pylint:disable=too-few-public-methods
    @abstractmethod
    def record_and_get_sample_rate(self, key: Hashable) -> int:
        """Record data and return a sample rate."""


class NoOpSampler(CustomSampler):
    """Sampling implementation that passes through everything."""
    # pylint:disable=too-few-public-methods

    def record_and_get_sample_rate(self, key: Hashable) -> int:
        return 1


class AverageWithMinimumSampler(CustomSampler):
    """Adaptive sampler.

    This implements essentially AvgSampleWithMin from
    honeycombio/dynsampler-go.

    Sample rates for keys are weighted proportionally to the log10 of
    their frequency. Everything is kept if fewer than
    min_events_per_second events are received in the period between
    the last two updates.

    """

    def __init__(
        self,
        *,
        goal_sample_rate: int = 10,
        min_events_per_second: int = 50,
        time_function=time.monotonic,
    ):
        """Initialize with settings."""
        self.goal_sample_rate = goal_sample_rate
        self.min_events_per_second = min_events_per_second
        self.time_function = time_function

        self.saved_sample_rates: Dict[Hashable, int] = {}
        self.current_counts: Counter = Counter()

        self.have_data = False

        self.last_update_time = self.time_function()

    def update(self) -> None:
        """Call periodically to update per-key sample rates."""
        # pylint: disable=too-many-locals
        update_time = self.time_function()
        update_interval = update_time - self.last_update_time
        self.last_update_time = update_time

        counts = self.current_counts
        self.current_counts = Counter()

        if not counts:
            self.saved_sample_rates = {}
            return

        sum_events = sum(counts.values())

        if sum_events < self.min_events_per_second * update_interval:
            self.saved_sample_rates = {key: 1 for key in counts}
            return

        new_saved_sample_rates = {}

        goal_count = sum_events / self.goal_sample_rate
        log_sum = sum(math.log10(c) for c in counts.values())

        try:
            goal_ratio = goal_count / log_sum
        except ZeroDivisionError:
            # Since goal_count is guaranteed to be > 0, this would be
            # the IEEE 754 behaviour.
            goal_ratio = float("inf")

        keys_remaining = len(counts)
        extra: float = 0

        for key in sorted(counts.keys()):
            count = counts[key]

            goal_for_key = max(1, math.log10(count) * goal_ratio)
            extra_for_key = extra / keys_remaining
            goal_for_key += extra_for_key
            extra -= extra_for_key
            keys_remaining -= 1

            if count < goal_for_key:
                new_saved_sample_rates[key] = 1
                extra += goal_for_key - count
            else:
                rate = math.ceil(count / goal_for_key)
                new_saved_sample_rates[key] = rate
                extra += goal_for_key - count / rate

        self.have_data = True
        self.saved_sample_rates = new_saved_sample_rates

    def record_and_get_sample_rate(self, key: Hashable) -> int:
        """Record the key and get the current sample rate for it."""
        self.current_counts[key] += 1

        if self.have_data:
            fallback_rate = 1
        else:
            fallback_rate = self.goal_sample_rate

        return self.saved_sample_rates.get(key, fallback_rate)


class TraceSampler(Sampler):
    """Adapted Beeline sampler, keeping a fraction of traces

    This implements sampling similar to dynamic sampling from the
    honeycomb-kubernetes-agent, with the exception of the root span of
    traces from requests with error responses all being kept.

    """

    def __init__(
        self,
        *,
        sample_rate: int,
        # We require a List rather than a Sequence, because str
        # matches Sequence[str], and a plain str is not what we want.
        partition_fields: List[str],
        min_events_per_second: int = 50,
        time_function=time.monotonic,
    ):

        self.partition_fields = partition_fields
        self.success_sampler = AverageWithMinimumSampler(
            goal_sample_rate=sample_rate,
            min_events_per_second=min_events_per_second,
            time_function=time_function,
        )
        self.error_sampler = NoOpSampler()

    def update(self) -> None:
        """Call periodically to run updates of sampler implementations."""
        self.success_sampler.update()

    def get_sampler(self, span_fields: Dict[str, Any]) -> CustomSampler:
        """Return the desired sampler for a type of trace."""

        try:
            if span_fields.get("status", 0) < 400:  # type:ignore
                # We don't know if sub-spans (that don't have a status
                # field) belong to a successful request, so the
                # sub-spans of a trace are always subject to sampling.
                return self.success_sampler
        except TypeError:
            pass
        return self.error_sampler

    def key(self, event_fields: Dict[str, Any]) -> Hashable:
        """Return a tuple of selected keys from event_fields.

        Tuple is based on our field partitions.
        """
        return tuple(str(event_fields.get(f)) for f in self.partition_fields)

    def sample(self, event_fields: Dict[str, Any]) -> Tuple[bool, int]:
        """Beeline event sampling callback function.

        Return wether an event should be sent, and also the effective
        inverted sample rate that is used.

        """
        sampler = self.get_sampler(event_fields)
        key = self.key(event_fields)
        sample_rate = sampler.record_and_get_sample_rate(key)

        # Sample deterministically based on trace id, so that either
        # all spans of a trace are kept, or none are.
        keep = should_keep(
            trace_id=event_fields["trace.trace_id"], sample_rate=sample_rate
        )

        return keep, sample_rate

    # pylint: disable=too-many-arguments
    def should_sample(
        self,
        parent_context: Optional[Context],
        trace_id: int,
        name: str,
        kind: Optional[trace.SpanKind] = None,
        attributes: Optional[Attributes] = None,
        links: Optional[Sequence[trace.Link]] = None,
        trace_state: Optional[TraceState] = None,
    ) -> SamplingResult:
        decision = Decision.DROP

        if attributes is not None:
            sampler = self.get_sampler(dict(attributes))
            key = self.key(dict(attributes))
            sample_rate = sampler.record_and_get_sample_rate(key)

            # Sample deterministically based on trace id, so that either
            # all spans of a trace are kept, or none are.
            if should_keep(trace_id=trace_id, sample_rate=sample_rate):
                decision = Decision.RECORD_AND_SAMPLE
            # Set the sample rate
            attributes = MappingProxyType(attributes | {"SampleRate": sample_rate})

        span = trace.get_current_span(parent_context)
        parent_span_context: Optional[trace.SpanContext] = span.get_span_context()

        trace_state = None
        if parent_span_context is not None:
            if parent_span_context.is_valid:
                trace_state = parent_span_context.trace_state
        # Sampling result third argument  has the wrong type...
        # It uses __init__(... trace_state: TraceState = None)  rather than
        # Optional[TraceState], thus we get a "may not be None"
        res = SamplingResult(decision, attributes, trace_state)  # type: ignore
        return res

    def get_description(self) -> str:
        return f"TraceSampler({self.partition_fields})"


def set_attribute_dicts(span, key_base: str, kwargs):
    """Set the attributes on span from values in the dict."""
    if not kwargs:
        return

    for key, val in kwargs.items():
        # None is not a valid value here, ignore it
        if val is None:
            continue
        if isinstance(val, dict):
            set_attribute_dicts(span, f"{key_base}.{key}", val)
        else:
            span.set_attribute(f"{key_base}.{key}", val)
