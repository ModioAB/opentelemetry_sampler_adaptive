"""Test cases for trace sampling."""
import pytest
import opentelemetry_sampler_adaptive as trace

# pylint:disable=missing-function-docstring,too-few-public-methods


@pytest.fixture(name="mock_time")
def fixture_mock_time():
    class MockTime:
        """Mocked time callable, like time.monotonic()."""

        def __init__(self):
            self.seconds = 0

        def __call__(self):
            return self.seconds

    return MockTime()


GOAL_SAMPLE_RATE = 10
MIN_EVENTS_PER_SECOND = 50
CONFIG_SAMPLE_RATE = 42


@pytest.fixture(name="avg_with_min_sampler")
def fixture_avg_with_min_sampler(mock_time):
    return trace.AverageWithMinimumSampler(
        goal_sample_rate=10, min_events_per_second=50, time_function=mock_time
    )


@pytest.fixture(name="trace_sampler")
def fixture_trace_sampler(mock_time):
    return trace.TraceSampler(
        sample_rate=CONFIG_SAMPLE_RATE,
        partition_fields=("status", "app.cid"),
        time_function=mock_time,
    )


def test_sampler_should_sample_at_goal_sample_rate_before_having_data(
    avg_with_min_sampler,
):
    rate = avg_with_min_sampler.record_and_get_sample_rate("alpha")
    assert rate == GOAL_SAMPLE_RATE


def test_sampler_should_not_sample_new_keys_when_having_data(
    avg_with_min_sampler, mock_time
):
    # Receive more than the minimum rate of events
    for _ in range(MIN_EVENTS_PER_SECOND * 2):
        avg_with_min_sampler.record_and_get_sample_rate("chi")
    mock_time.seconds = 1
    avg_with_min_sampler.update()

    # See "alpha" for the first time
    rate = avg_with_min_sampler.record_and_get_sample_rate("alpha")
    assert rate == 1


def test_sampler_should_not_sample_seen_keys_when_below_minimum_event_rate(
    avg_with_min_sampler, mock_time
):
    # See "alpha" first
    avg_with_min_sampler.record_and_get_sample_rate("alpha")

    # Receive more than the minimum rate of events
    for _ in range(MIN_EVENTS_PER_SECOND * 2):
        avg_with_min_sampler.record_and_get_sample_rate("chi")
    mock_time.seconds = 1
    avg_with_min_sampler.update()

    rate = avg_with_min_sampler.record_and_get_sample_rate("alpha")
    assert rate == 1


def test_sampler_should_sample_near_goal_sample_rate_for_single_keys(
    avg_with_min_sampler, mock_time
):
    # Receive more than the minimum rate of events
    for _ in range(MIN_EVENTS_PER_SECOND * 1000):
        avg_with_min_sampler.record_and_get_sample_rate("alpha")
    mock_time.seconds = 1
    avg_with_min_sampler.update()

    rate = avg_with_min_sampler.record_and_get_sample_rate("alpha")
    # Rounding may increase the sampling rate by 1 for some ranges of
    # event counts.
    assert GOAL_SAMPLE_RATE <= rate <= GOAL_SAMPLE_RATE + 1


def test_sampler_should_sample_at_goal_sample_rate_for_uniformly_distributed_keys(
    avg_with_min_sampler, mock_time
):
    for _ in range(MIN_EVENTS_PER_SECOND * 1000):
        avg_with_min_sampler.record_and_get_sample_rate("alpha")
        avg_with_min_sampler.record_and_get_sample_rate("beta")
        avg_with_min_sampler.record_and_get_sample_rate("gamma")
        avg_with_min_sampler.record_and_get_sample_rate("delta")
    mock_time.seconds = 1
    avg_with_min_sampler.update()

    for key in ("alpha", "beta", "gamma", "delta"):
        rate = avg_with_min_sampler.record_and_get_sample_rate(key)
        # Rounding may increase the sampling rate by 1 for some ranges
        # of event counts.
        assert GOAL_SAMPLE_RATE <= rate <= GOAL_SAMPLE_RATE + 1


def test_sampler_should_sample_more_of_less_common_keys(
    avg_with_min_sampler, mock_time
):
    for _ in range(MIN_EVENTS_PER_SECOND * 100):
        for _ in range(10):
            avg_with_min_sampler.record_and_get_sample_rate("alpha")
        avg_with_min_sampler.record_and_get_sample_rate("beta")
    mock_time.seconds = 1
    avg_with_min_sampler.update()

    # Sample rates should be weighted proportionally to the log10 of
    # their frequency. Note that higher rates means more dropped
    # events.
    rate = avg_with_min_sampler.record_and_get_sample_rate("alpha")
    assert rate == 17

    rate = avg_with_min_sampler.record_and_get_sample_rate("beta")
    assert rate == 2


def test_trace_sampler_should_not_sample_errors(trace_sampler, mock_time):
    # Receive more than the minimum rate of events
    for _ in range(MIN_EVENTS_PER_SECOND * 2):
        trace_sampler.should_sample(
            None, 1, "test", attributes={"trace.trace_id": "x", "status": 200}
        )
        trace_sampler.should_sample(
            None, 1, "test", attributes={"trace.trace_id": "x", "status": 400}
        )
    mock_time.seconds = 1
    trace_sampler.update()

    # Successes should be sampled near the goal sample rate
    decision = trace_sampler.should_sample(
        None, 1, "test", attributes={"trace.trace_id": "x", "status": 200}
    )
    rate = decision.attributes["SampleRate"]
    assert CONFIG_SAMPLE_RATE <= rate <= CONFIG_SAMPLE_RATE + 1

    # Failures should not be sampled
    decision = trace_sampler.should_sample(
        None, 1, "test", attributes={"trace.trace_id": "x", "status": 400}
    )
    rate = decision.attributes["SampleRate"]
    assert rate == 1


def test_trace_sampler_should_sample_more_of_less_common_requests(
    trace_sampler, mock_time
):
    for _ in range(MIN_EVENTS_PER_SECOND * 100):
        for _ in range(10):
            trace_sampler.should_sample(
                None,
                1,
                "test",
                attributes={
                    "trace.trace_id": "x",
                    "status": 200,
                    "app.cid": "ffffffffffff",
                },
            )
        trace_sampler.should_sample(
            None,
            1,
            "test",
            attributes={
                "trace.trace_id": "x",
                "status": 200,
                "app.cid": "000000000001",
            },
        )
    mock_time.seconds = 1
    trace_sampler.update()

    # Sample rates should be weighted proportionally to the log10 of
    # their frequency. Note that higher rates means more dropped
    # events.
    decision = trace_sampler.should_sample(
        None,
        1,
        "test",
        attributes={"trace.trace_id": "x", "status": 200, "app.cid": "ffffffffffff"},
    )
    assert decision.attributes["SampleRate"] == 67

    decision = trace_sampler.should_sample(
        None,
        1,
        "test",
        attributes={"trace.trace_id": "x", "status": 200, "app.cid": "000000000001"},
    )
    assert decision.attributes["SampleRate"] == 9


def test_trace_sampler_should_handle_different_types_on_paritition_fields(
    trace_sampler,
):
    trace_sampler.should_sample(
        None,
        1,
        "test",
        attributes={"trace.trace_id": "x", "status": 200, "app.cid": "y"},
    )
    trace_sampler.should_sample(
        None,
        1,
        "test",
        attributes={"trace.trace_id": "x", "status": 200, "app.cid": None},
    )
    trace_sampler.update()
