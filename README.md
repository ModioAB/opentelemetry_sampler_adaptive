# More adaptive samplers for opentelemetry

This code was ported from our custom samplers in beelines in order to have some
more control over which fields are used when sampling. 

- Prevent a single spammy user from dominating fields 
   ( per unique field trace ratio, ie keep a % of traces based on a unique selector )

- Prevent rare errors from disappearing
  ( certain keys in the traces will make it be kept all the time)

Thee porting has been done with part of an axe, but comments etc. has not all
been replaced.
